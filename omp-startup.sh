#!/bin/bash
sed -i -e "s/host = localhost/host = ${OJS_DB_HOST}/g" /var/www/omp/config.inc.php
sed -i -e "s/username = omp/username = ${OJS_DB_USER}/g" /var/www/omp/config.inc.php
sed -i -e "s/password = omp/password = ${OJS_DB_PASSWORD}/g" /var/www/omp/config.inc.php
sed -i -e "s/name = omp/name = ${OJS_DB_NAME}/g" /var/www/omp/config.inc.php
sed -i -e "s/public_files_dir = public/public_files_dir = \/var\/www\/public/g" /var/www/omp/config.inc.php

sed -i -e "s/\/var\/www\/html/\/var\/www\/omp/g" /etc/apache2/sites-available/000-default.conf
sed -i -e "s/www.example.com/${SERVERNAME}/g" /etc/apache2/sites-available/000-default.conf
sed -i -e "s/\/var\/log\/apache2/${APACHE_LOG_DIR}/g" /etc/apache2/sites-available/000-default.conf
sed -i -e "s/error.log/%Y-%m-%d+${LOG_NAME}-error.log/g" /etc/apache2/sites-available/000-default.conf
sed -i -e "s/access.log/%Y-%m-%d+${LOG_NAME}-access.log/g" /etc/apache2/sites-available/000-default.conf

# Run the apache process in the foreground as in the php image
echo "[OJS Startup] Starting apache..."
apache2-foreground
