Dockerizing OMP (Open Monograph Press)OJS
=========================================

Open Monograph Press is an open source software platform for managing the editorial workflow required to see monographs, edited volumes and, scholarly editions through internal and external review, editing, cataloguing, production, and publication. OMP can operate, as well, as a press website with catalog, distribution, and sales capacities.

###How to use this image###
```
$docker run -p 80:80 --link some-mysql:mysql -d infrascielo/omp-ojs
```
* -e OMP_DB_HOST=... (default top localhost. Must be change to IP and port of the linked mysql container)
* -e OMP_DB_USER=... (default to omp)
* -e OMP_DB_PASSWORD=... (default to omp)
* -e OMP_DB_NAME=... (default to omp)
* -e SERVERNAME=...(default to omp.scielo.org)

To work with --link you must have to run a mysql container
```
docker run --name <some-mysql> \
           -e MYSQL_ROOT_PASSWORD=<password>  \
           -e MYSQL_USER=<user> \
           -e MYSQL_PASSWORD=<password> \
           -d mysql
```

The instruction above creates container setting mysql root password and creates other user and its respective password.

After create mysql container you can can run the omp container.

**Sample**
```
$ docker run --name mysql \
              -e MYSQL_ROOT_PASSWORD=omp  \
              -e MYSQL_USER=omp \
              -e MYSQL_PASSWORD=omp \
              -d mysql
```
```
$ docker run -p 80:80 \
             -e OMP_DB_HOST=mysql \
             -e OMP_DB_USER=root \
             -e OMP_DB_PASSWORD=omp \
             --link mysql:mysql \
             -d infrascielo/omp-ojs
```

Using SystemD to manage containers
==================================
[omp.service](https://bitbucket.org/infra-scielo/omp-ojs/raw/2e42727f3f849f96105abc4777e8919ca7b06266/omp.service)
```
[Unit]
Description=Docker Application Container Engine
Documentation=https://docs.docker.com
After=network.target docker.socket
Requires=docker.socket

[Service]
TimeoutStartSec=0
ExecStartPre=/usr/bin/docker pull infrascielo/omp-ojs
ExecStartPre=-/usr/bin/docker rm infrascielo/omp-ojs
ExecStart=/usr/bin/docker run \
          --rm \
          --name %p \
          -v <APACHE LOG DIR>:/var/log/apache2 \
          -v <OMP FILES DIR>:/var/www/files \
          -v <OMP PUBLIC DIR>:/var/www/public \
          -p 80:80 \
          --link mysql:mysql \
          infrascielo/omp-ojs

ExecStop=/usr/bin/docker stop %p
ExecStopPost=-/usr/bin/docker rm %p

[Install]
WantedBy=multi-user.target
```

You must change to the directory of your choice. Each of them will be in different directory.
```<APACHE LOG DIR>``` 
```<OMP FILES DIR>```
```<OMP PUBLIC DIR>```

[mysql.service](https://bitbucket.org/infra-scielo/omp-ojs/raw/2e42727f3f849f96105abc4777e8919ca7b06266/mysql.service)
```
[Unit]
Description=Docker Mysql Application
Documentation=https://docs.docker.com
After=network.target docker.service
Requires=docker.service

[Service]
TimeoutStartSec=0
ExecStartPre=/usr/bin/docker pull mysql
ExecStartPre=-/usr/bin/docker rm mysql
ExecStart=/usr/bin/docker run \
          --rm \
          --name %p \
          -e MYSQL_ROOT_PASSWORD=<password> \
          -e MYSQL_USER=<user> \
          -e MYSQL_PASSWORD=<password> \
          -v <MYSQL DATA DIR>:/var/lib/mysql \
          mysql

ExecStop=/usr/bin/docker stop %p
ExecStopPost=-/usr/bin/docker rm %p

[Install]
WantedBy=multi-user.target
```

You must change to the directory of your choice. Each of them will be in different directory.
```<MYSQL DATA DIR>```

[mysql-dump.service](https://bitbucket.org/infra-scielo/omp-ojs/raw/2e42727f3f849f96105abc4777e8919ca7b06266/mysql-dump.service)
```
[Unit]
Description=Docker Mysql Application
Documentation=https://docs.docker.com
After=network.target mysql.service
Requires=docker.service

[Service]
TimeoutStartSec=0
StandardInput=tty-force
ExecStart=/bin/sh -c "\
          /usr/bin/docker run \
          -it \
          --link mysql:mysql \
          --rm \
          mysql \
          sh -c 'exec mysqldump -h <CONTAINER_MYSQL_NAME> -P 3306 -u<MYSQL_USER> -p<MYSQL_PASSWORD> omp' > <MYSQL_DUMP_DIR>/omp.sql"

[Install]
WantedBy=multi-user.target
```

You must change to the directory of your choice. Each of them will be in different directory.
```<CONTAINER_MYSQL_NAME>```
```<MYSQL_USER>```
```<MYSQL_PASSWORD>```
```<MYSQL_DUMP_DIR>```

[mysql-dump.timer](https://bitbucket.org/infra-scielo/omp-ojs/raw/2e42727f3f849f96105abc4777e8919ca7b06266/mysql-dump.timer)
```
[Unit]
Description=Run MySQL Dump Daily

[Timer]
OnCalendar=*-*-* 21:00:00

[Install]
WantedBy=timers.target
```
This unit timer will be triggered everyday at 21 o'clock.

Don't forget to enable all systemd unit:
```
`$sudo systemctl daemon-reload
`$sudo systemctl enable omp.service mysql.service mysql-dump.service mysql-dump.time
```
